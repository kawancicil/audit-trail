#!/bin/sh
bulan=(13 12 11 10 9 8 7 6 5 4 3 2)

for bln in "${bulan[@]}"
do

echo -e "+++ Last Access `date '+%B %Y' -d "$bln month ago"` +++\n"
#Last User Activities
last | grep -e "`date +%b -d "$bln month ago"`"
echo -e "\n=== +++ ===\n"

done